export interface ApiResponse {
  result: boolean;
  data?: any;
}
