export interface Post {
  id?: number;
  title?: string;
  description?: string;
  body?: string;
  userId: number;
  edit?: boolean;
}
