import { Injectable } from '@angular/core';
import { Observable, throwError, pipe } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Update } from '@ngrx/entity';
import { Store, select } from '@ngrx/store';

import * as fromPostsActions from '../../modules/posts/store/actions/posts.actions';
import { selectPosts } from '../../modules/posts/store/selectors/posts.selectors';
import { PostsState } from '../../modules/posts/store/reducers/posts.reducer';

import { ApiResponse } from '@interfaces/api-response';
import { Post } from '@interfaces/post';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  postsApiUrl = 'https://jsonplaceholder.typicode.com/posts';

  constructor(
    private store: Store<PostsState>,
    private httpClient: HttpClient
  ) { }

  loadPosts(): Observable<Post[]> {
    return this.httpClient.get<Post[]>( this.postsApiUrl );
  }

  getPosts():Observable<Post[]> {
    this.store.dispatch( fromPostsActions.loadPosts() );
    return this.store.pipe( select( selectPosts ) );
  }

  addPost(post: Post){
    this.store.dispatch( fromPostsActions.addPost({post}) );
  }

  insertPost( post: Post ):Observable<ApiResponse>  {
    const headers = new HttpHeaders()
      .set("Content-Type", "application/json");

    let response: ApiResponse = {result: false, data: null};

    return this.httpClient
      .post<ApiResponse>(this.postsApiUrl, post, {headers: headers})
      .pipe(
        map( (result: any) => {
          response.result = true;
          response.data = result.id;
          return response;
        }),
        catchError(error => {
          response.data = 'Adding post error';
          return throwError(response);
        })
      );
  }

  editPost(post: Update<Post>):void {
    this.store.dispatch( fromPostsActions.updatePost({post}) );
  }

  updatePost(post: Update<Post>) {
    const headers = new HttpHeaders()
      .set("Content-Type", "application/json");

    let response: ApiResponse = {result: false, data: null};

    return this.httpClient
      .put<ApiResponse>(`${this.postsApiUrl}/${post.id}`, post, {headers: headers})
      .pipe(
        map( (result: any) => {
          response.result = true;
          response.data = result.id;
          return response;
        }),
        catchError(error => {
          response.data = 'Update post error';
          return throwError(response);
        })
      );
  }

  deletePost(post: Post):void  {
    this.store.dispatch( fromPostsActions.removePost({post}) );
  }

  removePost(post: Post) {
    let response: ApiResponse = {result: false, data: null};

    return this.httpClient
      .delete<ApiResponse>(`${this.postsApiUrl}/${post.id}`)
      .pipe(
        map( (result: any) => {
          response.result = true;
          response.data = result.id;
          return response;
        }),
        catchError(error => {
          response.data = 'Delete post error';
          return throwError(response);
        })
      );
  }

}
