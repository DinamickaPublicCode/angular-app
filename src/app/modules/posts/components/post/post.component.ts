import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Post } from '@interfaces/post';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnInit {

  @Input() post: Post;
  @Output() onEdit: EventEmitter<Post> = new EventEmitter(null);
  @Output() onDelete: EventEmitter<Post> = new EventEmitter(null);

  postForm: FormGroup;

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.createForm();
  }

  createForm():void {
    this.postForm = this.fb.group({
      id: [this.post.id,[Validators.required]],
      userId: [this.post.userId,[Validators.required]],
      edit: [false,[Validators.required]],
      title: [this.post.title,[Validators.required]],
      body: [this.post.body,[Validators.required]]
    });
  }

  delete(post: Post):void {
    this.onDelete.next(post);
  }

  update(post: Post):void {
    Object.keys(this.postForm.value).forEach( key => {
      post[key] = this.postForm.value[key];
    });
    this.onEdit.next(post);
  }

}
