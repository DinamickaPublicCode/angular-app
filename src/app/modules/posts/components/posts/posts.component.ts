import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Update } from '@ngrx/entity';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ToastrService } from 'ngx-toastr';

import { PostsService } from '@services/posts.service';
import { Post } from '@interfaces/post';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.sass']
})
export class PostsComponent implements OnInit {

  postForm: FormGroup;
  posts$: Observable<Post[]>;
  nextId: number;
  userId: number;

  formErrors = {
    "title": "",
    "body": "",
  };

  validationMessage = {
    "title": {
      "required": "Required field.",
      "minlength": "Min length is 6 characters",
      "maxlength": "Max length is 32 characters"

    },
    "body": {
      "required": "Required field.",
      "minlength": "Min length is 12 characters",
      "maxlength": "Max length is 256 characters"
    }
  }

  constructor(
    private fb: FormBuilder,
    private postsService: PostsService,
    private toastrService: ToastrService
  ) { }

  ngOnInit() {
    this.getPosts();
    this.createForm();
  }

  createForm():void {
    this.postForm = this.fb.group({
      title: [null, [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(32),
      ]],
      body: [null, [
        Validators.required,
        Validators.minLength(12),
        Validators.maxLength(500),
      ]],
      userId: [1,[]],
      id: [null,[]]
    });

    this.postForm.valueChanges
      .subscribe(data => this.onValueChange(data))

  }

  getPosts():void {
    this.posts$ = this.postsService.getPosts();
    this.posts$.subscribe( posts => {
      this.nextId = posts.length + 1;
      if (posts[0]) {
        this.userId = posts[0].userId;
      }
    });
  }

  addPost(post: Post):void {
    this.postsService.addPost( post );
  }

  onDelete(post: Post):void {
    this.postsService.deletePost(post);
  }

  onEdit(post: Update<Post>):void {
    this.postsService.editPost(post);
  }

  onValueChange(data?:any){
    if (!this.postForm) return;
    let form = this.postForm;

    for (let field in this.formErrors){
      this.formErrors[field] = "";
      let control = form.get(field);

      if (control && control.dirty && !control.valid){
        let message = this.validationMessage[field];
        for (let key in control.errors){
          this.formErrors[field] += message[key] + " ";
        }
      }
    }
  }

  onSubmit(){
    if (!this.postForm.invalid) {
      this.postsService.addPost({
        title: this.postForm.value.title,
        body: this.postForm.value.body,
        userId: this.userId,
        id: this.nextId
      });
      this.postForm.reset();
    }
    else {
      this.toastrService.error('Please, fill in required fields', 'Send post fail!');
    }
  }
}
