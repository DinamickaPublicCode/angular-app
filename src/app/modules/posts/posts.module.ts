import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms'
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule }  from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from '@shared/shared.module';
import { PostsRoutingModule } from './posts-routing.module';

import * as fromPosts from './store/reducers/posts.reducer';
import { PostEffects } from './store/effects/posts.effects';

import { PostsComponent } from './components/posts/posts.component';
import { PostComponent } from './components/post/post.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(fromPosts.postsFeatureKey, fromPosts.reducer),
    EffectsModule.forFeature([PostEffects]),
    ReactiveFormsModule,
    PostsRoutingModule,
  ],
  declarations: [PostsComponent, PostComponent]
})
export class PostsModule { }
