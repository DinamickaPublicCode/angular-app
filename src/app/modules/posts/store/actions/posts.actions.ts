import { createAction, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { Post } from '@interfaces/post';
import { ApiResponse } from '@interfaces/api-response';

export const loadPosts = createAction(
  '[Posts] Load Posts'
);

export const loadPostsSuccess = createAction(
  '[Posts] Load Posts Success',
  props<{ posts: Post[] }>()
);

export const addPost = createAction(
  '[Posts] Add Post',
  props<{ post: Post }>()
);

export const addPostSuccess = createAction(
  '[Posts] Add Post Success',
  props<{ response: ApiResponse }>()
);

export const updatePost = createAction(
  '[Posts] Update Post',
  props<{ post: Update<Post> }>()
);

export const updatePostSuccess = createAction(
  '[Posts] Update Post Success',
  props<{ response: ApiResponse }>()
);

export const removePost = createAction(
  '[Posts] Remove Post',
  props<{ post: Post }>()
);

export const removePostSucces = createAction(
  '[Posts] Remove Post Success',
  props<{ response: ApiResponse }>()
);

export const loadPostsFailure = createAction(
  '[Posts] Load Posts Failure',
  props<{ error: any }>()
);
