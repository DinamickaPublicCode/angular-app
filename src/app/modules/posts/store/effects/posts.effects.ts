import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of, EMPTY } from 'rxjs';
import { map, catchError, mergeMap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import { Post } from '@interfaces/post';
import { ApiResponse } from '@interfaces/api-response';
import { PostsService } from '@services/posts.service';
import * as fromPostsActions from '../actions/posts.actions';

@Injectable()
export class PostEffects {

  loadPosts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromPostsActions.loadPosts),
      mergeMap( () => this.postsService.loadPosts().pipe(
        map( posts => fromPostsActions.loadPostsSuccess({ posts })
        ),
        catchError(error => of(fromPostsActions.loadPostsFailure({ error })))
      ))
    )
  );

  removePost$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromPostsActions.removePost),
      mergeMap( (action) => this.postsService.removePost(action.post).pipe(
        map((response: ApiResponse) => {
          if (response.result) {
            this.toastrService.success(`Post removed`);
          } else {
            this.toastrService.error(response.data);
          }
          return fromPostsActions.removePostSucces({ response })
        }),
        catchError(error => of(fromPostsActions.loadPostsFailure({ error })))
      ))
    )
  );

  updatePost$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromPostsActions.updatePost),
      mergeMap( (action) => this.postsService.updatePost(action.post).pipe(
        map((response: ApiResponse) => {
          if (response.result) {
            this.toastrService.success(`Post updated: ${response.data}`);
          } else {
            this.toastrService.error(response.data);
          }
          return fromPostsActions.updatePostSuccess({ response })
        }),
        catchError(error => of(fromPostsActions.loadPostsFailure({ error })))
      ))
    )
  );

  addPost$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromPostsActions.addPost),
      mergeMap( (action) => this.postsService.insertPost(action.post).pipe(
        map((response: ApiResponse) => {
          if (response.result) {
            this.toastrService.success(`Added new post id of: ${response.data}`);
          } else {
            this.toastrService.error(response.data);
          }
          return fromPostsActions.addPostSuccess({ response })
        }),
        catchError(error => of(fromPostsActions.loadPostsFailure({ error })))
      ))
    )
  );

  constructor(
    private actions$: Actions,
    private postsService: PostsService,
    private toastrService: ToastrService,
    ) {}

}
