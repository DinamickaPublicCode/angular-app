import { Action, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter, Update } from '@ngrx/entity';

import * as fromPostsActions from '../actions/posts.actions';
import { Post } from '@interfaces/post';

export const postsFeatureKey = 'posts';

export interface PostsState extends EntityState<Post>{
  error: any;
}

export function sortByDate(a: Post, b: Post): number {
  return b.id - a.id
}

export const postsAdapter:EntityAdapter<Post> = createEntityAdapter<Post>({
  sortComparer: sortByDate
});

export const todoInitialState = postsAdapter.getInitialState({
  error: null
});

export const reducer = createReducer(
  todoInitialState,
  on(
    fromPostsActions.loadPostsSuccess,
    ( state, action ) => postsAdapter.addAll( action.posts, state )
  ),
  on(
    fromPostsActions.addPost,
    (state, action) => postsAdapter.addOne( action.post, state )
  ),
  on(
    fromPostsActions.removePost,
    (state, action) => postsAdapter.removeOne( action.post.id, state )
  ),
  on(
    fromPostsActions.updatePost,
    (state, action) => postsAdapter.updateOne( action.post, state )
  )
);
