import { createFeatureSelector, createSelector } from '@ngrx/store';
import { postsFeatureKey, PostsState, postsAdapter } from '../reducers/posts.reducer';

export const selectPostsState = createFeatureSelector<PostsState>(postsFeatureKey);

export const selectPosts = createSelector(
  selectPostsState,
  postsAdapter.getSelectors().selectAll
);

export const selectError = createSelector(
  selectPostsState,
  (state: PostsState) => state.error
);
